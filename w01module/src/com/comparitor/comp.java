package com.comparitor;

//import com.company.WhatNowClass;

import java.util.*;
import java.lang.*;

// A class to represent a student.
class Planet
{
    int planetID;
    String name;
    String attributes;

    // Constructor
    public Planet(int planetID, String planetName, String attributes)
    {
        this.planetID = planetID;
        this.name = planetName;
        this.attributes = attributes;
    }

    // Used to print Planets details in main()
    public String toString()
    {
        return this.planetID + " | " + this.name + " | " + this.attributes;
    }
}

class SortByID implements Comparator<Planet>
{
    // Used for sorting in ascending order of
    // roll number
    public int compare(Planet a, Planet b)
    {
        return a.planetID - b.planetID;
    }
}

class SortByName implements Comparator<Planet>
{
    // Used for sorting in ascending order of
    // name
    public int compare(Planet a, Planet b)
    {
        return a.name.compareTo(b.name);
    }
}

// Driver class
class Main
{
    public static void main (String[] args)
    {
        ArrayList<Planet> myPlanets = new ArrayList<>();
        myPlanets.add(new Planet(301, "sun", "That's no moon."));
        myPlanets.add(new Planet(112, "mercury", "Is this the smallest?"));
        myPlanets.add(new Planet(213, "venus", "hot and whatnot"));
        myPlanets.add(new Planet(114, "earth", "3rd Rock From The Sun"));
        myPlanets.add(new Planet(115, "mars", "Matt Damon Sciences the heck out of it"));
        myPlanets.add(new Planet(216, "jupiter", "Gassy"));
        myPlanets.add(new Planet(117, "saturn", "Put a ring on it!"));
        myPlanets.add(new Planet(318, "uranus", "something"));
        myPlanets.add(new Planet(119, "neptune", "tilted"));
        myPlanets.add(new Planet(120, "pluto", "Did you heard about Pluto? That's messed up, right?"));

        System.out.println("\nDefault Unsorted (in entered order");
        for (Planet value : myPlanets) System.out.println(value);

        myPlanets.sort(new SortByID());

        System.out.println("\nSorted by planetID");
        for (Planet planet : myPlanets) System.out.println(planet);

        myPlanets.sort(new SortByName());

        System.out.println("\nSorted by planetName");
        for (Planet myPlanet : myPlanets) System.out.println(myPlanet);
    }
}
