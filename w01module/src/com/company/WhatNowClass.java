package com.company;
//whatnow class is used in the main class
public class WhatNowClass {
    private Integer id;
    private String planet;
    private String attributes;

    public WhatNowClass(Integer id, String planet, String attributes) {
        this.id = id;
        this.planet = planet.toUpperCase();
        this.attributes = attributes;
    }

    public String toString() {
        return "ID: " + id + " | Planet: " + planet + " | Attributes: " + attributes;
    }
}