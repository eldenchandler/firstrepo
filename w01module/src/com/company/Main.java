package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("\n----- Start Collections: List-ArrayList -----");

        //new constructor builds an empty array list
        List<String> myList = new ArrayList<>();
        try {
            //populate list
            myList.add("sun");
            myList.add("mercury");
            myList.add("venus");
            myList.add("earth");
            myList.add("mars");
            myList.add("jupiter");
            myList.add("saturn");
            myList.add("uranus");
            myList.add("neptune");
            myList.add("pluto");
            //myList.add(1); //for testing
        } catch (Exception err) {
            System.out.println(err.getMessage());
            System.out.println("Fix code in Collections: List");
        } finally {
            int myIterator = 1;
            //display list on 1 line in brackets
            System.out.println(myList.size() + " items in list: \n" + myList + "\n");

            //iterate through displaying list
            for (Object myString : myList) {
                System.out.println(myIterator + ": " + myString); //"(String)" is needed to cast to string.
                ++myIterator;
            }
        }

        System.out.println("\n----- Start Collections: Set-TreeSet -----");
        //tree set is sorting alphabetically
        //new constructor builds an empty array list
        Set<String> mySet = new TreeSet<>();
        try {
            //populate list
            mySet.add("sun");
            mySet.add("mercury");
            mySet.add("venus");
            mySet.add("earth");
            mySet.add("mars");
            mySet.add("jupiter");
            mySet.add("saturn");
            mySet.add("uranus");
            mySet.add("neptune");
            mySet.add("pluto");
            //mySet.add(1); //for testing
        } catch (Exception err) {
            System.out.println(err.getMessage());
            System.out.println("Fix code in Collections: Set");
        } finally {
            int myIterator = 1;
            //display list on 1 line in brackets
            System.out.println(mySet.size() + " items in set: \n" + mySet + "\n");

            //iterate through displaying list
            for (Object myString : mySet) {
                System.out.println(myIterator + ": " + myString); //"(String)" is needed to cast to string.
                ++myIterator;
            }
            System.out.println("\nComparators");

        }

        System.out.println("\n----- Start Collections: Queue-PriorityQueue -----");
        //priority queue is sorting alphabetically
        //new constructor builds an empty array list
        Queue<String> myQueue = new PriorityQueue<>();
        try {
            //populate list
            myQueue.add("sun");
            myQueue.add("mercury");
            myQueue.add("venus");
            myQueue.add("earth");
            myQueue.add("mars");
            myQueue.add("jupiter");
            myQueue.add("saturn");
            myQueue.add("uranus");
            myQueue.add("neptune");
            myQueue.add("pluto");
            //myList.add(1); //for testing
        } catch (Exception err) {
            System.out.println(err.getMessage());
            System.out.println("Fix code in Collections: Queue");
        } finally {
            int myIterator = 1;
            //display list on 1 line in brackets
            System.out.println(myQueue.size() + " items in list: \n" + myQueue + "\n");

            //iterate through displaying list
            Iterator<String> thisIterator = myQueue.iterator();
            while (thisIterator.hasNext()) {
                System.out.println(myIterator + ": " + myQueue.poll()); //"(String)" is needed to cast to string.
                ++myIterator;
            }
        }

        System.out.println("\n----- Start Collections: Map-HashMap -----");
        // maps use key pairs
        //new constructor builds an empty array list
        Map<Integer, String> myMap = new HashMap<>();
        try {
            //populate list
            myMap.put(11, "sun");
            myMap.put(2, "mercury");
            myMap.put(3, "venus");
            myMap.put(4, "earth");
            myMap.put(5, "mars");
            myMap.put(6, "jupiter");
            myMap.put(7, "saturn");
            myMap.put(8, "uranus");
            myMap.put(9, "neptune");
            myMap.put(10, "pluto");
            //myMap.put(11,1); //for testing
        } catch (Exception err) {
            System.out.println(err.getMessage());
            System.out.println("Fix code in Collections: Map");
        } finally {
            int myIterator = 1;
            //display list on 1 line in brackets
            System.out.println(myMap.size() + " items in map: \n" + myMap + "\n");

            //iterate through displaying list
            for (int i = 1; i <= 10; i++) {
                String myResult = myMap.get(i);
                System.out.println(myIterator + ": " + myResult); //"(String)" is needed to cast to string.
                ++myIterator;
            }
        }

        System.out.println("\n----- Start Collections: Tree-TreeMap -----");
        //TreeMap use key value pairs. sorts by the position
        //new constructor builds an empty array list
        TreeMap<Integer, String> myTreeMap = new TreeMap<>();
        try {
            //populate list
            myTreeMap.put(10,"sun");
            myTreeMap.put(1, "mercury");
            myTreeMap.put(2, "venus");
            myTreeMap.put(3, "earth");
            myTreeMap.put(4,"mars");
            myTreeMap.put(5,"jupiter");
            myTreeMap.put(6,"saturn");
            myTreeMap.put(7,"uranus");
            myTreeMap.put(8, "neptune");
            myTreeMap.put(9,"pluto");
            //myTreeMap.put(1); //for testing
        } catch (Exception err) {
            System.out.println(err.getMessage());
            System.out.println("Fix code in Collections: TreeMap");
        } finally {
            int myIterator = 1;
            //display list on 1 line in brackets
            System.out.println(myTreeMap.size() + " items in list: \n" + myTreeMap + "\n");

            //Iteration example

            for (Integer treeMapIterator : myTreeMap.keySet()) {
                System.out.println(treeMapIterator + ": " + myTreeMap.get(treeMapIterator));
                ++myIterator;
            }
        }

        System.out.println("\n----- Start Collections: \"Generics\" -----");
        List<WhatNowClass> aList = new LinkedList<>();
        try {
            aList.add(new WhatNowClass(111, "sun", "That's no moon."));
            aList.add(new WhatNowClass(112, "mercury", "Is this the smallest?"));
            aList.add(new WhatNowClass(113, "venus", "hot and whatnot"));
            aList.add(new WhatNowClass(114, "earth", "3rd Rock From The Sun"));
            aList.add(new WhatNowClass(115, "mars", "Matt Damon Sciences the heck out of it"));
            aList.add(new WhatNowClass(116, "jupiter", "Gassy"));
            aList.add(new WhatNowClass(117, "saturn", "Ringy"));
            aList.add(new WhatNowClass(118, "uranus", "something"));
            aList.add(new WhatNowClass(119, "neptune", "tilted"));
            aList.add(new WhatNowClass(120, "pluto", "Did you heard about Pluto? That's messed up, right?"));
        } catch (Exception err) {
            System.out.println(err.getMessage());
            System.out.println("Fix code in Collections: Generics");
        } finally {
            for (WhatNowClass myOutput : aList) {
                System.out.println(myOutput);
            }
        }
    }
}
